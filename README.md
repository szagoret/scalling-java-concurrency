# Scaling Java Applications Through Concurrency #

### Description ###
Distributed systems and multi-core platforms are far too prevalent now to dismiss everyday concurrency. 
This course outlines several concurrency patterns that the Java Concurrency API simplifies, 
making it much easier to scale your Java application.